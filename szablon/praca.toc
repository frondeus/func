\select@language {polish}
\select@language {polish}
\contentsline {chapter}{\numberline {1}Wst\IeC {\k e}p}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Cechy i za\IeC {\l }o\IeC {\.z}enia j\IeC {\k e}zyka}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}P\IeC {\k e}tla REPL}{3}{section.2.1}
\contentsline {section}{\numberline {2.2}Gramatyka j\IeC {\k e}zyka}{4}{section.2.2}
\contentsline {section}{\numberline {2.3}Wyra\IeC {\.z}enia I32, NIL oraz TRUE}{5}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Przyk\IeC {\l }ad dzia\IeC {\l }ania}{5}{subsection.2.3.1}
\contentsline {section}{\numberline {2.4}Wyra\IeC {\.z}enie let}{5}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Wielokrotne deklarowanie zmiennych}{5}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Przyk\IeC {\l }ad u\IeC {\.z}ycia zmiennej wewn\IeC {\k a}trz deklaracji}{6}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Przeszukiwanie zakres\IeC {\'o}w lokalnych}{6}{subsection.2.4.3}
\contentsline {section}{\numberline {2.5}Wyra\IeC {\.z}enie cond}{7}{section.2.5}
\contentsline {section}{\numberline {2.6}Wyra\IeC {\.z}enie lambda}{7}{section.2.6}
\contentsline {subsection}{\numberline {2.6.1}Domkni\IeC {\k e}cia}{8}{subsection.2.6.1}
\contentsline {section}{\numberline {2.7}Wyra\IeC {\.z}enie var}{8}{section.2.7}
\contentsline {section}{\numberline {2.8}Wyra\IeC {\.z}enie primCall}{9}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Funkcje wbudowane}{9}{subsection.2.8.1}
\contentsline {subsubsection}{atom?}{9}{subsubsection*.1}
\contentsline {subsubsection}{inc}{9}{subsubsection*.2}
\contentsline {subsubsection}{dec}{9}{subsubsection*.3}
\contentsline {subsubsection}{not}{10}{subsubsection*.4}
\contentsline {subsubsection}{and}{10}{subsubsection*.5}
\contentsline {subsubsection}{or}{10}{subsubsection*.6}
\contentsline {subsubsection}{Operatory arytmetyczne}{10}{subsubsection*.7}
\contentsline {subsubsection}{Operatory relacyjne}{10}{subsubsection*.8}
\contentsline {subsubsection}{Cons}{10}{subsubsection*.9}
\contentsline {subsubsection}{Car}{10}{subsubsection*.10}
\contentsline {subsubsection}{Cdr}{11}{subsubsection*.11}
\contentsline {section}{\numberline {2.9}Wyra\IeC {\.z}enie call}{11}{section.2.9}
\contentsline {section}{\numberline {2.10}Rekurencja}{11}{section.2.10}
\contentsline {subsection}{\numberline {2.10.1}Rekurencja ogonowa}{12}{subsection.2.10.1}
\contentsline {chapter}{\numberline {3}Architektura kompilatora i narz\IeC {\k e}dzi}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Za\IeC {\l }o\IeC {\.z}enia}{13}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Przebiegi kompilatora}{13}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Maszyna wirtualna}{14}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Przypadki u\IeC {\.z}ycia i scenariusze}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Diagramy klas}{16}{section.3.3}
\contentsline {section}{\numberline {3.4}Diagramy aktywno\IeC {\'s}ci}{19}{section.3.4}
\contentsline {section}{\numberline {3.5}Opis j\IeC {\k e}zyk\IeC {\'o}w po\IeC {\'s}rednich oraz bajtokodu}{21}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}J\IeC {\k e}zyk po\IeC {\'s}redni 1}{21}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}J\IeC {\k e}zyk po\IeC {\'s}redni 2}{21}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}J\IeC {\k e}zyk po\IeC {\'s}redni 3}{21}{subsection.3.5.3}
\contentsline {subsection}{\numberline {3.5.4}J\IeC {\k e}zyk po\IeC {\'s}redni 4}{22}{subsection.3.5.4}
\contentsline {subsection}{\numberline {3.5.5}Bajtokod}{23}{subsection.3.5.5}
\contentsline {section}{\numberline {3.6}Algorytm analizuj\IeC {\k a}cy wyst\IeC {\k e}powanie zmiennych nielokalnych}{24}{section.3.6}
\contentsline {chapter}{Bibliografia}{25}{chapter*.18}
