extern crate funclib;
extern crate runtime;
extern crate env_logger;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn lambdas() {
    assert_driver("((x, y) => +(x, y))(5, 6)", &Value::I32(11));
}

#[test]
fn lambdas_cond() {
    assert_driver(
        "((y) => 
            cond {
                nil => 6,
                #t => +(5, y)
            })(6)
        ",
        &Value::I32(11),
    );
}

#[test]
fn lambdas_cond_2() {
    assert_driver(
        "let foo = ((y) => cond {
                nil => 6,
                #t => +(5, y)
            })
         in foo(6)",
        &Value::I32(11),
    );
}


#[test]
fn lambdas_let() {
    assert_driver("let foo = (x, y) => +(x, y) in foo(5, 6)", &Value::I32(11));
}

#[test]
fn lambdas_let_2() {
    assert_driver(
        "(() => (let x = 5
            in +(x, 6)
        ))()",
        &Value::I32(11),
    );
}

#[test]
fn lambdas_let_3() {
    assert_driver(
        "let 
            foo = ((x) => +(1, x)),
            bar = (() => foo(5))
        in bar()",
        &Value::I32(6),
    );
}

#[test]
fn lambdas_let_4() {
    assert_driver(
        "let 
            foo = ((x) => +(1, x)),
            bar = (() => +(foo(5), foo(6)))
        in bar()",
        &Value::I32(13),
    );
}

#[test]
fn lambdas_let_5() {
    assert_driver(
        "let foo = 
            (x) => +(1, x)
        in 
        +(foo(5), foo(6))",
        &Value::I32(13),
    );
}

#[test]
fn closures() {
    assert_driver("(let x = 5 in (y) => +(x, y))(6)", &Value::I32(11));
}

#[test]
fn closures_2() {
    assert_driver(
        "
        let foo = (
            let bar = (
                let x = 5 
                in (y) => (
                    () => +(x, y)
                )
            )
            in bar(6)
        )
        in foo()
        ",
        &Value::I32(11),
    );
}

#[test]
fn closures_2_5() {
    assert_driver(
        " let foo = (
            let bar = (y) => (
                let x = 5 in () => +(x, y)
            )
            in bar(6)
        )
        in foo()",
        &Value::I32(11),
    );
}

#[test]
fn closures_3() {
    assert_driver(
        "
        let bar = (
            let x = 5 in (
                let l3 = (y) => (
                    let l2 = () => (
                        let z = 10 in (
                            let l1 = () => (
                                +(+(x, y), z) 
                            )
                            in l1
                        )
                    )
                    in l2
               ) in l3
           )
       )
       in ((bar(6))())() 
        ",
        &Value::I32(5 + 6 + 10),
    );
}
