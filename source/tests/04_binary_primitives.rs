extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn add() {
    assert_driver("+(2, 2)", &Value::I32(4));
    assert_driver("+(5, 2)", &Value::I32(7));
    assert_driver("+(2, 5)", &Value::I32(7));
    assert_driver("+(2, -5)", &Value::I32(-3));
    assert_driver("+(-2, -5)", &Value::I32(-7));
}

#[test]
fn sub() {
    assert_driver("-(5, 2)", &Value::I32(3));
    assert_driver("-(0, 2)", &Value::I32(-2));
    assert_driver("-(2, 5)", &Value::I32(-3));
    assert_driver("-(-5, -2)", &Value::I32(-3));
}

#[test]
fn mul() {
    assert_driver("*(5, 2)", &Value::I32(10));
    assert_driver("*(5, -2)", &Value::I32(-10));
    assert_driver("*(-5, 2)", &Value::I32(-10));
    assert_driver("*(-5, -2)", &Value::I32(10));
}

#[test]
fn div() {
    assert_driver("/(10, 2)", &Value::I32(5));
    assert_driver("/(16, 2)", &Value::I32(8));
    assert_driver("/(11, 2)", &Value::I32(5));
}

#[test]
fn equals() {
    assert_driver("==(#t, #t)", &Value::True);
    assert_driver("==(nil, #t)", &Value::Nil);
    assert_driver("==(#t, nil)", &Value::Nil);
    assert_driver("==(nil, nil)", &Value::True);
    assert_driver("==(1, 1)", &Value::True);
    assert_driver("==(3, 1)", &Value::Nil);
}

#[test]
fn less() {
    assert_driver("<(1, 1)", &Value::Nil);
    assert_driver("<(10, 1)", &Value::Nil);
    assert_driver("<(1, 2)", &Value::True);
}

#[test]
fn less_or_eq() {
    assert_driver("<=(1, 1)", &Value::True);
    assert_driver("<=(10, 1)", &Value::Nil);
    assert_driver("<=(1, 2)", &Value::True);
}

#[test]
fn greater() {
    assert_driver(">(1, 1)", &Value::Nil);
    assert_driver(">(10, 1)", &Value::True);
    assert_driver(">(1, 2)", &Value::Nil);
}

#[test]
fn greater_or_eq() {
    assert_driver(">=(1, 1)", &Value::True);
    assert_driver(">=(10, 1)", &Value::True);
    assert_driver(">=(1, 2)", &Value::Nil);
}

#[test]
fn and() {
    assert_driver("and(#t, #t)", &Value::True);
    assert_driver("and(nil, #t)", &Value::Nil);
    assert_driver("and(#t, nil)", &Value::Nil);
    assert_driver("and(nil, nil)", &Value::Nil);
}

#[test]
fn or() {
    assert_driver("or(#t, #t)", &Value::True);
    assert_driver("or(nil, #t)", &Value::True);
    assert_driver("or(#t, nil)", &Value::True);
    assert_driver("or(nil, nil)", &Value::Nil);
}
