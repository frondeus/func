extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn is_atom_for_true() {
    assert_driver("atom?(#t)", &Value::True);
    //assert_driver("(atom? #t)", &Value::True);
}

#[test]
fn is_atom_for_nil_atom() {
    assert_driver("atom?(nil)", &Value::True);
}

#[test]
fn is_atom_for_number() {
    assert_driver("atom?(1)", &Value::True);
}

#[test]
#[ignore]
fn is_atom_for_atom() {
    //assert_driver("(atom? 'x)", &Value::True);
    unimplemented!("Needs quote");
}

#[test]
fn add1_for_integer() {
    assert_driver("inc(0)", &Value::I32(1));
    assert_driver("inc(1)", &Value::I32(2));
    assert_driver("inc(-1)", &Value::I32(0));
}

#[test]
fn sub1_for_integer() {
    assert_driver("dec(0)", &Value::I32(-1));
    assert_driver("dec(1)", &Value::I32(0));
    assert_driver("dec(-1)", &Value::I32(-2));
    assert_driver("dec(5)", &Value::I32(4));
}

#[test]
fn not() {
    assert_driver("not(#t)", &Value::Nil);
    assert_driver("not(nil)", &Value::True);
}
