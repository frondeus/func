extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn local_variable_1() {
    assert_driver("let x = 5 in x", &Value::I32(5));
}

#[test]
fn local_variable_2() {
    assert_driver("let x = 10 in +(x, 5)", &Value::I32(15));
}

#[test]
fn local_variable_3() {
    assert_driver("let x = -2 in +(5, x)", &Value::I32(3));
}

#[test]
fn multiple_variables() {
    assert_driver("let x = 1, z = 5, y = 2 in +(y, x)", &Value::I32(3));
}

#[test]
#[should_panic(expected = "variable `y` not found")]
fn local_scope() {
    assert_driver("let x = 1 in y", &Value::I32(0));
}

#[test]
fn name_collision() {
    assert_driver("let x = 1 , x = 2 in +(x, x)", &Value::I32(4));
}

#[test]
fn vars_from_vars() {
    assert_driver("let x = 1 , y = inc(x) in +(x, y)", &Value::I32(3));
}
