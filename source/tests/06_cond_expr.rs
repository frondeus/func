extern crate funclib;
extern crate runtime;

//extern crate timebomb;

use runtime::value::Value;
use funclib::driver::assert_driver;

//use timebomb::timeout_ms;
#[test]
fn cond_nil() {
    assert_driver(
        "cond {
                nil => 1
    }",
        &Value::Nil,
    );
}

#[test]
fn cond_1() {
    assert_driver(
        "cond {
                    nil => 1,
                    #t => 2
                  }",
        &Value::I32(2),
    );
    //assert_driver("(cond (nil 1) (#t 2))", &Value::I32(2));
}

#[test]
fn cond_2() {
    assert_driver(
        "cond {
                    atom?(1) => 5,
                    #t => 6
                  }",
        &Value::I32(5),
    );
    //assert_driver("(cond ((atom? 1) 5) (#t 6))", &Value::I32(5));

}

#[test]
fn cond_3() {
    //assert_driver("(cond (#t 4))", &Value::I32(4));
    assert_driver(
        "cond {
                    #t => 4
                  }",
        &Value::I32(4),
    );
}

#[test]
fn cond_let() {
    assert_driver(
        "cond {
        #t => let x = 5 in x,
        nil => let x = 1 in x
        }",
        &Value::I32(5),
    );
    /*
    assert_driver(
        "(cond 
    (#t  (let ((x 5)) x))
    (nil (let ((x 1)) x)))",
        &Value::I32(5),
    );
    */
}

#[test]
fn cond_let_2() {
    assert_driver(
        "cond {
            nil => let x = 5 in x,
            #t => let x = 1 in x
        }",
        &Value::I32(1),
    );
    /*
    assert_driver(
        "(cond 
    (nil  (let ((x 5)) x))
    (#t (let ((x 1)) x)))",
        &Value::I32(1),
    );
    */
}

#[test]
fn cond_let_3() {
    assert_driver(
        "let x = 3 in cond {
            nil => let y = 5 in +(x, y),
            #t => let y = 7 in -(y, x)
        }",
        &Value::I32(4),
    );
    /*
    assert_driver(
        r#"(let ((x 3))
            (cond
                (nil (let ((y 5)) (+ x y)))
                (#t (let ((y 7)) (- y x)))))"#,
        &Value::I32(4),
    );
    */
}
