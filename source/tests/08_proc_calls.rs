extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;
//use driver::assert_driver_bt;

#[test]
#[ignore]
fn label() {
    assert_driver(
        "(labels (
            (f0 (code (x y) () (+ x y)))
            ) nil)",
        &Value::Nil,
    );
}

#[test]
#[ignore]
#[should_panic(expected = "expected code")]
fn label_no_code() {
    assert_driver(
        "(labels ((f0 (mode (x y) () (+ x y)))) (+ 5 6))",
        &Value::Nil,
    );
}

#[test]
#[ignore]
fn call() {
    assert_driver(
        "(labels (
            (f0 (code (x y) () (- x y)))
            ) (f0 10 6))",
        &Value::I32(4),
    );
}
