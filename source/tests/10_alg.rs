
extern crate funclib;
extern crate runtime;
extern crate env_logger;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn fib() {
    env_logger::init().unwrap();

    assert_driver(
        "
        let fib = (x) => 
            cond {
                ==(x, 0) => 0,
                ==(x, 1) => 1,
                #t => +(fib(dec(x)), fib(-(x,2)))
            }
        in
        fib(10)",
        &Value::I32(55),
    );
}

#[test]
fn tail_fib() {
    assert_driver(
        "let fib = (n) => 
             let fib2 = (n, a, b) =>
                 cond {
                     ==(n, 0) => b,
                     #t => fib2(dec(n), +(a, b), a)
                 }
             in fib2(n, 1, 0)
        in fib(10)
        ",
        &Value::I32(55),
    )
}
