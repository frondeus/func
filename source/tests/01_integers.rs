extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn positive() {
    assert_driver("1234", &Value::I32(1234));
}

#[test]
fn negative() {
    assert_driver("-1234", &Value::I32(-1234));
}
