extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn cons_1() {
    assert_driver("cons(10, 20)", &Value::Cons(0, 1));
    //assert_driver("(cons 10 20)", Value::Cons(0, 1));
}

#[test]
fn cons_2() {
    assert_driver("cons(10, cons(20, nil))", &Value::Cons(0, 3));
    //assert_driver("(cons 10 (cons 20 nil))", Value::Cons(0, 3));
}

#[test]
fn car() {
    assert_driver("car(cons(10,20))", &Value::I32(10));
    //assert_driver("(car (cons 10 20))", Value::I32(10));
}

#[test]
fn cdr() {
    assert_driver("cdr(cons(10,20))", &Value::I32(20));
    //assert_driver("(cdr (cons 10 20))", Value::I32(20));
}

#[test]
fn car_2() {
    assert_driver("car(cons(cons(10, 20), cons(30, 40)))", &Value::Cons(0, 1));
    //assert_driver("(car (cons (cons 10 20) (cons 30 40)))", Value::Cons(0, 1));
}

#[test]
fn cdr_2() {
    assert_driver("cdr(cons(cons(10, 20), cons(30, 40)))", &Value::Cons(3, 4));
    //assert_driver("(cdr (cons (cons 10 20) (cons 30 40)))", Value::Cons(3, 4));
}

/*
mod higher {
    use runtime::value::Value;
    use funclib::driver::assert_driver;

    #[test]
    fn caar() {
        assert_driver("(caar (cons (cons 10 20) (cons 30 40)))", Value::I32(10));
    }

    #[test]
    fn cdar() {
        assert_driver("(cdar (cons (cons 10 20) (cons 30 40)))", Value::I32(20));
    }

    #[test]
    fn cadr() {
        assert_driver("(cadr (cons (cons 10 20) (cons 30 40)))", Value::I32(30));
    }

    #[test]
    fn cddr() {
        assert_driver("(cddr (cons (cons 10 20) (cons 30 40)))", Value::I32(40));
    }
}
*/
