extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
fn atom_nil() {
    assert_driver("nil", &Value::Nil);
}

#[test]
fn list_true() {
    assert_driver("#t", &Value::True);
}

#[test]
#[ignore]
fn symbol() {
    //assert_driver("x", &Value::Symbol(1));
    unimplemented!("Needs quote");
}
