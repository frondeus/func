extern crate funclib;
extern crate runtime;

use runtime::value::Value;
use funclib::driver::assert_driver;

#[test]
#[ignore]
fn labels() {
    assert_driver(
        "(labels ((f0 (code (x y) () (+ x y)))) (+ 1 2))",
        &Value::I32(3),
    );
}

#[test]
#[ignore]
fn closure() {
    assert_driver(
        "(labels ((f0 (code () (x y) (+ x y)))
                (f1 (code (y) (x) (closure f0 x y))))
         (let ((x 5)) (closure f1 x)))",
        &Value::Address(0),
    );
}

#[test]
#[ignore]
fn call() {
    assert_driver(
        r#"
(let (
    (ff0 
        (labels (
                (f0 (code (x) (y) (+ x y)))
            )
            (let ((w 5) (z 7)) (closure f0 w))
        )
    ))
    (ff0 6)
)"#,
        &Value::I32(5 + 6),
    );
}
