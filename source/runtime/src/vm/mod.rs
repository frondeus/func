use bytecode::Bytecode;
use value::Value;

pub trait Printer {
    fn print(&mut self, value: Value);
}

pub struct VirtualMachine {
    frame_pointer: usize,
    program_counter: usize,
    allocation_pointer: usize,
    closure_pointer: usize,
    stack: Vec<Value>,
    pub heap: Vec<Value>,
}

impl VirtualMachine {
    pub fn new() -> VirtualMachine {
        VirtualMachine::new_with_heap(vec![Value::Nil; 10])
    }

    pub fn new_with_heap(heap: Vec<Value>) -> VirtualMachine {
        VirtualMachine {
            frame_pointer: 0,
            allocation_pointer: 0,
            closure_pointer: 0,
            program_counter: 0,
            stack: vec![],
            heap: heap,
        }
    }

    pub fn eval<P: Printer>(&mut self, printer: &mut P, code: Vec<Bytecode>, entry_point: usize) {
        self.program_counter = entry_point;
        loop {
            let cmd = &code[self.program_counter];
            self.next_cmd();

            debug!("CMD: {:?}", cmd);

            match *cmd {
                Bytecode::Halt => return,

                Bytecode::Pop => {
                    self.pop();
                }
                Bytecode::PushNil => self.push(Value::Nil),
                Bytecode::PushTrue => self.push(Value::True),
                Bytecode::PushI32(i) => self.push(Value::I32(i)),
                Bytecode::PushAddress(a) => self.push(Value::Address(a)),
                Bytecode::PushSymbol(s) => self.push(Value::Symbol(s)),
                Bytecode::PushClosure(l, a) => self.push(Value::Closure(l, a)),

                Bytecode::Print => printer.print(self.pop()),
                Bytecode::Add => self.add(), 
                Bytecode::Sub => self.sub(),
                Bytecode::Mul => self.mul(),
                Bytecode::Div => self.div(), 
                Bytecode::And => self.and(), 
                Bytecode::Or => self.or(),

                Bytecode::Add1 => self.add1(),
                Bytecode::Sub1 => self.sub1(),
                Bytecode::Not => self.not(),

                Bytecode::Call(argc) => self.call(argc),
                Bytecode::Return => self.return_call(),

                Bytecode::LoadClosure => self.load_closure(),

                Bytecode::LoadArg(offset) => self.load_arg(offset),
                Bytecode::Load(offset) => self.load(offset),
                Bytecode::Save(offset) => self.save(offset),

                Bytecode::LoadMem => self.load_mem(),
                Bytecode::LoadMemOffset(offset) => self.load_mem_offset(offset),
                Bytecode::SaveMem => self.save_mem(),
                Bytecode::GetAddr => self.get_addr(),

                Bytecode::Jump(address) => self.jump(address),
                Bytecode::SkipCond => self.skip_cond(),

                Bytecode::IsAtom => self.is_atom(),
                Bytecode::IsEqual => self.is_equal(),
                Bytecode::IsLess => self.is_less(),
                Bytecode::IsGreater => self.is_greater(),
                Bytecode::IsLessOrEq => self.is_less_or_eq(),
                Bytecode::IsGreaterOrEq => self.is_greater_or_eq(),
                Bytecode::Car => self.car(),
                Bytecode::Cdr => self.cdr(),
                Bytecode::Cons => self.cons(),
            }

            debug!("STACK: {:?} \nHEAP: {:?}\n", self.stack, self.heap);
        }
    }

    fn push(&mut self, value: Value) {
        self.stack.push(value);
    }

    pub fn pop(&mut self) -> Value {
        self.stack.pop().unwrap()
    }

    fn pop_many(&mut self, size: usize) {
        for _ in 0..size {
            self.pop();
        }
    }

    fn get(&self, address: usize) -> &Value {
        self.stack.get(address).unwrap()
    }


    fn next_cmd(&mut self) {
        self.program_counter += 1;
    }

    fn is_atom(&mut self) {
        let value = self.pop();
        let result = value.is_atom();

        self.push(Value::from(result));
    }

    fn is_equal(&mut self) {
        let b = self.pop();
        let a = self.pop();

        if !a.is_atom() || !b.is_atom() {
            self.push(Value::Nil);
            return;
        }

        let result = a == b;

        self.push(Value::from(result));
    }

    fn is_less(&mut self) {
        let b = self.pop();
        let a = self.pop();

        if !a.is_atom() || !b.is_atom() {
            self.push(Value::Nil);
            return;
        }

        let result = a < b;

        self.push(Value::from(result));
    }

    fn is_greater(&mut self) {
        let b = self.pop();
        let a = self.pop();

        if !a.is_atom() || !b.is_atom() {
            self.push(Value::Nil);
            return;
        }

        let result = a > b;

        self.push(Value::from(result));
    }

    fn is_less_or_eq(&mut self) {
        let b = self.pop();
        let a = self.pop();

        if !a.is_atom() || !b.is_atom() {
            self.push(Value::Nil);
            return;
        }

        let result = a <= b;

        self.push(Value::from(result));
    }

    fn is_greater_or_eq(&mut self) {
        let b = self.pop();
        let a = self.pop();

        if !a.is_atom() || !b.is_atom() {
            self.push(Value::Nil);
            return;
        }

        let result = a >= b;

        self.push(Value::from(result));
    }

    fn car(&mut self) {
        let (car_adr, _) = self.pop().expect_cons();

        self.push(Value::Address(car_adr));
    }

    fn cdr(&mut self) {
        let (_, cdr_adr) = self.pop().expect_cons();

        self.push(Value::Address(cdr_adr));
    }

    fn cons(&mut self) {
        let cdr = self.pop().expect_address();
        let car = self.pop().expect_address();

        self.push(Value::Cons(car, cdr));
    }

    fn add(&mut self) {
        let b = self.pop().expect_i32();
        let a = self.pop().expect_i32();
        self.push(Value::I32(a + b));
    }

    fn sub(&mut self) {
        let b = self.pop().expect_i32();
        let a = self.pop().expect_i32();
        self.push(Value::I32(a - b));
    }

    fn mul(&mut self) {
        let b = self.pop().expect_i32();
        let a = self.pop().expect_i32();
        self.push(Value::I32(a * b));
    }

    fn div(&mut self) {
        let b = self.pop().expect_i32();
        let a = self.pop().expect_i32();
        self.push(Value::I32(a / b));
    }

    fn add1(&mut self) {
        let a = self.pop().expect_i32();
        self.push(Value::I32(a + 1));
    }

    fn sub1(&mut self) {
        let a = self.pop().expect_i32();
        self.push(Value::I32(a - 1));
    }

    fn not(&mut self) {
        let a = self.pop();
        if a.is_nil() {
            self.push(Value::True);
        } else {
            self.push(Value::Nil);
        }
    }

    fn and(&mut self) {
        let b = self.pop();
        let a = self.pop();

        if !a.is_nil() && !b.is_nil() {
            self.push(Value::True);
        } else {
            self.push(Value::Nil);
        }
    }

    fn or(&mut self) {
        let b = self.pop();
        let a = self.pop();

        if !a.is_nil() || !b.is_nil() {
            self.push(Value::True);
        } else {
            self.push(Value::Nil);
        }
    }


    fn load_closure(&mut self) {
        let a = self.closure_pointer;

        self.push(Value::Address(a));
    }

    fn load_arg(&mut self, offset: usize) {
        let argc_pointer = self.frame_pointer as i32 - 4;

        let argc = self.get(argc_pointer as usize).expect_argc();

        if offset > argc {
            panic!(
                "This procedure has {} arguments. You are trying to get {} argument. Wrong!",
                argc,
                offset
            );
        }

        let value_offset = offset as i32 - argc as i32 - 4;

        self.load(value_offset)
    }

    fn load(&mut self, offset: i32) {
        let fp_begin = self.frame_pointer as i32;

        let value_offset = (fp_begin + offset as i32) as usize;
        let value = self.get(value_offset).clone();

        self.push(value);
    }

    fn save(&mut self, offset: i32) {
        let fp_begin = self.frame_pointer as i32;

        let value_offset = (fp_begin + offset as i32) as usize;
        let value = self.pop();

        if value_offset >= self.stack.len() {
            error!(
                "Stack save: len is {} but index is {}",
                self.stack.len(),
                value_offset
            );
            panic!();
        }
        self.stack[value_offset] = value;
    }

    fn call(&mut self, argc: usize) {
        let closure_address = self.pop().expect_address();

        let argc = Value::InternalArgc(argc);
        let fp = Value::InternalFramePointer(self.frame_pointer);
        let pc = Value::InternalProgramCounter(self.program_counter);
        let cp = Value::InternalClosurePointer(self.closure_pointer);

        self.push(argc);
        self.push(fp);
        self.push(pc);
        self.push(cp);

        self.closure_pointer = closure_address;
        self.frame_pointer = self.stack.len(); // - 1;

        let proc_address = self.heap[closure_address].expect_closure().0;
        self.jump(proc_address);
    }

    fn return_call(&mut self) {
        let return_value = self.pop();
        let stack_diff = self.stack.len() - self.frame_pointer; // - 1;

        self.pop_many(stack_diff);

        let cp = self.pop().expect_closure_pointer();
        let pc = self.pop().expect_program_counter();
        let fp = self.pop().expect_frame_pointer();
        let argc = self.pop().expect_argc();

        self.jump(pc);
        self.frame_pointer = fp;

        self.closure_pointer = cp;

        self.pop_many(argc);
        self.push(return_value);
    }

    fn load_mem(&mut self) {
        let addr = self.pop().expect_address();

        let value = self.heap[addr].clone();

        self.push(value);
    }

    fn load_mem_offset(&mut self, offset: i32) {
        let addr = (self.pop().expect_address() as i32 + offset) as usize;

        let value = self.heap[addr].clone();
        self.push(value);
    }

    fn save_mem(&mut self) {
        let pointer = self.allocation_pointer;
        self.allocation_pointer += 1;

        let value = self.pop();

        self.heap[pointer] = value;
    }

    fn get_addr(&mut self) {
        let pointer = self.allocation_pointer - 1;
        self.push(Value::Address(pointer));
    }

    fn jump(&mut self, address: usize) {
        self.program_counter = address;
    }

    fn skip_cond(&mut self) {
        let value = self.pop();
        if !value.is_nil() {
            self.next_cmd();
        }
    }
}
