#[derive(Clone, Debug, PartialEq, PartialOrd)]


pub enum Value {
    Nil,
    True,
    Symbol(i32),
    Cons(usize, usize),
    I32(i32),
    Address(usize),

    Closure(usize, usize),

    InternalProgramCounter(usize),
    InternalFramePointer(usize),
    InternalClosurePointer(usize),
    InternalArgc(usize),
}

impl From<bool> for Value {
    fn from(value: bool) -> Value {
        if value { Value::True } else { Value::Nil }
    }
}

impl Value {
    pub fn is_nil(&self) -> bool {
        match *self {
            Value::Nil => true,
            _ => false,
        }
    }

    pub fn is_atom(&self) -> bool {
        match *self {
            Value::InternalFramePointer(_) => panic!("Internal value cannot be used in is_atom"),
            Value::InternalArgc(_) => panic!("Internal value cannot be used in is_atom"),
            Value::InternalProgramCounter(_) => panic!("Internal value cannot be used in is_atom"),
            Value::InternalClosurePointer(_) => panic!("Internal value cannot be used in is_atom"),
            Value::Address(_) => panic!("Memory address cannot be used in is_atom"),
            Value::Cons(_, _) => false,
            _ => true,
        }
    }

    pub fn expect_i32(&self) -> i32 {
        match *self {
            Value::I32(i) => return i,
            _ => panic!("Expected i32"),
        }
    }

    pub fn expect_cons(&self) -> (usize, usize) {
        match *self {
            Value::Cons(car, cdr) => return (car, cdr),
            _ => panic!("Expected cons"),
        }
    }

    pub fn expect_program_counter(&self) -> usize {
        match *self {
            Value::InternalProgramCounter(u) => return u,
            _ => panic!("Expected program counter"),
        }
    }

    pub fn expect_frame_pointer(&self) -> usize {
        match *self {
            Value::InternalFramePointer(u) => return u,
            _ => panic!("Expected frame pointer"),
        }
    }

    pub fn expect_closure_pointer(&self) -> usize {
        match *self {
            Value::InternalClosurePointer(u) => return u,
            _ => panic!("Expected closure pointer"),
        }
    }

    pub fn expect_argc(&self) -> usize {
        match *self {
            Value::InternalArgc(u) => return u,
            _ => panic!("Expected internal argc"),
        }
    }

    pub fn expect_address(&self) -> usize {
        match *self {
            Value::Address(u) => return u,
            _ => panic!("Expected memory address"),
        }
    }

    pub fn expect_closure(&self) -> (usize, usize) {
        match *self {
            Value::Closure(addr, argc) => return (addr, argc),
            _ => panic!("Expected closure"),
        }
    }
}
