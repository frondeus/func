#[macro_use]
extern crate log;

pub mod bytecode;
pub mod value;
pub mod vm;

#[cfg(test)]
mod tests_vm {
    use bytecode::Bytecode;
    use vm::VirtualMachine;
    use value::Value;
    use vm::Printer;

    struct TestPrinter {
        results: Vec<Value>,
    }

    impl Printer for TestPrinter {
        fn print(&mut self, value: Value) {
            info!("Print: {:?}", value);

            self.results.push(value);
        }
    }

    impl TestPrinter {
        fn new() -> TestPrinter {
            TestPrinter { results: vec![] }
        }

        fn assert_last(&mut self, value: Value) {
            assert_eq!(self.results.pop().unwrap(), value);
        }
    }

    #[test]
    fn push_nil() {
        let mut printer = TestPrinter::new();

        let bytes = vec![Bytecode::PushNil, Bytecode::Print, Bytecode::Halt];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::Nil);
    }

    #[test]
    fn push_true() {
        let mut printer = TestPrinter::new();

        let bytes = vec![Bytecode::PushTrue, Bytecode::Print, Bytecode::Halt];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::True);
    }

    #[test]
    fn push_i32() {
        let mut printer = TestPrinter::new();

        let bytes = vec![Bytecode::PushI32(10), Bytecode::Print, Bytecode::Halt];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(10));
    }

    #[test]
    fn add() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::PushI32(08),
            Bytecode::Add,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(18));
    }

    #[test]
    fn sub() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::PushI32(08),
            Bytecode::Sub,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(2));
    }

    #[test]
    fn jump() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushNil,
            Bytecode::Jump(3),
            Bytecode::PushI32(10),
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::Nil);
    }

    #[test]
    fn skip_cond() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushNil,
            Bytecode::SkipCond,
            Bytecode::PushI32(10),
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(10));
    }

    #[test]
    fn skip_cond2() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::PushI32(233),
            Bytecode::SkipCond,
            Bytecode::PushNil,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(10));
    }


    #[test]
    fn function_call() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            // Add and sub 3 numbers
            Bytecode::LoadArg(0), //10
            Bytecode::LoadArg(1), //5
            Bytecode::LoadArg(2), //3
            Bytecode::Add,
            Bytecode::Sub,
            Bytecode::Return,

            Bytecode::PushI32(10),
            Bytecode::PushI32(5),
            Bytecode::PushI32(3),

            Bytecode::PushI32(15),
            Bytecode::PushClosure(0, 1),
            Bytecode::SaveMem,
            Bytecode::GetAddr,
            Bytecode::Load(1),
            Bytecode::SaveMem,

            Bytecode::Call(3),
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(6).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(2));
    }

    #[test]
    fn is_atom() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushNil,
            Bytecode::IsAtom,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::True);
    }


    #[test]
    fn is_atom2() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::IsAtom,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::True);
    }

    #[test]
    fn is_atom3() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushAddress(1),
            Bytecode::PushAddress(2),
            Bytecode::Cons,
            Bytecode::IsAtom,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::Nil);
    }

    #[test]
    fn is_equal() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushNil,
            Bytecode::PushNil,
            Bytecode::IsEqual,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::True);
    }

    #[test]
    fn is_equal2() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::PushNil,
            Bytecode::IsEqual,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::Nil);
    }

    #[test]
    fn is_equal3() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::PushI32(10),
            Bytecode::IsEqual,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::True);
    }

    #[test]
    fn car() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::SaveMem,
            Bytecode::GetAddr,
            Bytecode::PushI32(5),
            Bytecode::SaveMem,
            Bytecode::GetAddr,

            Bytecode::Cons,

            Bytecode::Car,
            Bytecode::LoadMem,

            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(10));
    }

    #[test]
    fn cdr() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::SaveMem,
            Bytecode::GetAddr,
            Bytecode::PushI32(5),
            Bytecode::SaveMem,
            Bytecode::GetAddr,

            Bytecode::Cons,

            Bytecode::Cdr,
            Bytecode::LoadMem,

            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(5));
    }

    impl VirtualMachine {
        pub fn assert_heap(&self, address: usize, value: Value) {
            assert_eq!(self.heap[address], value);
        }
    }
    #[test]
    fn save_mem() {
        let mut printer = TestPrinter::new();

        let bytes = vec![Bytecode::PushI32(1234), Bytecode::SaveMem, Bytecode::Halt];

        let mut vm = VirtualMachine::new(0);
        vm.eval(&mut printer, bytes);

        vm.assert_heap(0, Value::I32(1234));
    }

    #[test]
    fn load_mem() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushAddress(2),
            Bytecode::LoadMem,
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new_with_heap(
            0,
            vec![Value::Nil, Value::Nil, Value::I32(1234), Value::Nil],
        ).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(1234));
    }

    #[test]
    fn save_cons() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(10),
            Bytecode::SaveMem,
            Bytecode::GetAddr,
            Bytecode::PushI32(5),
            Bytecode::SaveMem,
            Bytecode::GetAddr,

            Bytecode::Cons,

            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(0).eval(&mut printer, bytes);

        printer.assert_last(Value::Cons(0, 1));
    }

    #[ignore]
    #[test]
    fn save() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::PushI32(0),
            Bytecode::PushI32(5),
            Bytecode::PushI32(12),
            Bytecode::Save(0),
            Bytecode::PushI32(32),
            Bytecode::Load(0),
            Bytecode::Print,
            Bytecode::Return,
            //Bytecode::Call(0, 0),
            Bytecode::Halt,
        ];

        VirtualMachine::new(8).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(12));
    }

    #[test]
    fn closure() {
        let mut printer = TestPrinter::new();

        let bytes = vec![
            Bytecode::LoadClosure,
            Bytecode::LoadMemOffset(0),
            Bytecode::LoadClosure,
            Bytecode::LoadMemOffset(1),
            Bytecode::Add,
            Bytecode::Return,

            Bytecode::PushClosure(0, 2),
            Bytecode::SaveMem,
            Bytecode::GetAddr,
            Bytecode::LoadClosure,
            Bytecode::LoadMemOffset(0),
            Bytecode::SaveMem,
            Bytecode::LoadArg(0),
            Bytecode::SaveMem,
            Bytecode::Return,

            Bytecode::PushI32(7),
            Bytecode::PushI32(5),
            Bytecode::PushClosure(6, 1),
            Bytecode::SaveMem,
            Bytecode::GetAddr,
            Bytecode::Load(1),
            Bytecode::SaveMem,
            Bytecode::Call(1),
            Bytecode::Print,
            Bytecode::Halt,
        ];

        VirtualMachine::new(15).eval(&mut printer, bytes);

        printer.assert_last(Value::I32(12));
    }
}
