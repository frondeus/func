#[derive(Clone, Debug, PartialEq)]
pub enum Bytecode {
    Halt,

    Return,
    Call(usize), // argc

    Pop,

    PushI32(i32),
    PushAddress(usize),
    PushSymbol(i32),
    PushClosure(usize, usize),
    PushNil,
    PushTrue,

    Jump(usize),
    SkipCond,

    LoadArg(usize),
    Load(i32),
    Save(i32),

    LoadClosure,

    LoadMem,
    LoadMemOffset(i32),
    SaveMem,
    GetAddr,

    Print,
    Add,
    Sub,
    Add1,
    Sub1,
    Not,
    And,
    Or,
    Mul,
    Div,

    IsAtom,
    IsEqual,
    IsLess,
    IsGreater,
    IsLessOrEq,
    IsGreaterOrEq,
    Car,
    Cdr,
    Cons,
}
