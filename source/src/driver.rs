use nom::IResult;

use ast::AST;
use parser::expr;
use compiler::Compiler;
use runtime::bytecode::Bytecode;
use runtime::vm::VirtualMachine;
use runtime::vm::Printer;
use runtime::value::Value;

fn parse_expr(input: &str) -> AST {
    use std::str;
    use nom::Err::Position;

    let parse_result = expr(input.as_bytes());
    match parse_result {
        IResult::Done(completed, ast) => {
            //if !completed.is_empty() {
                //error!("incomplete: {:?}", str::from_utf8(completed).unwrap());
            //}
            assert_eq!(completed, &b";"[..]);
            ast
        }
        IResult::Error(err) => match err {
            Position(ref pos, input) => {
                let msg = str::from_utf8(input).unwrap();
                panic!("Error in: {} at {:?}", msg, pos);
            }
            ref el => panic!("{:?}", el)
        }
        _ => panic!("Expected done parse"),
    }
}

pub fn compile(input: &str) -> (Vec<Bytecode>, usize) {
    let ast = parse_expr(input);

    let mut compiler = Compiler::new();

    compiler.compile(&[ast]).unwrap();

    let mut bytes = compiler.output.clone();
    let entry_point = compiler.entry_point;

    bytes.push(Bytecode::Print);
    bytes.push(Bytecode::Halt);

    (bytes, entry_point)
}

pub fn assert_driver_bt(input: &str) {
    let bytes = compile(input).0;
    panic!("Bytes: {:?}", bytes);
}

pub fn assert_driver(input: &str, value: &Value) {
    debug!("INPUT: {:?}", input);

    let completed_input = String::from(input) + ";";
    let output = compile(&completed_input);
    let bytes = output.0;

    let first_byte = output.1;
    let mut printer = TestPrinter::new();


    VirtualMachine::new().eval(&mut printer, bytes, first_byte);

    printer.assert_last(value);
}

struct TestPrinter {
    results: Vec<Value>,
}

impl Printer for TestPrinter {
    fn print(&mut self, value: Value) {
        info!("Print: {:?}", value);

        self.results.push(value);
    }
}

impl TestPrinter {
    fn new() -> TestPrinter {
        TestPrinter { results: vec![] }
    }

    fn assert_last(&mut self, value: &Value) {
        assert_eq!(
            !self.results.is_empty(),
            true,
            "printer needs at least one print"
        );
        assert_eq!(self.results.pop().unwrap(), *value);
    }
}
