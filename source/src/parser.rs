use std::str;

use ast::AST;

named!(symbol<&str>, map_res!(is_not!("(,;{}) \t\n\r"), str::from_utf8));

named!(boxed<Box<AST> >, map!(expr, Box::new));
named!(pub expr<AST>, ws!(alt!(
        s_i32 
        | s_nil
        | s_true
        | prim_call
        | s_let
        | s_cond
        | s_lambda
        | unknown_atom
        | func_call
        | parent
        )));

named!(parent<AST>, ws!(delimited!(tag!("("), expr, tag!(")"))));
named!(s_i32<AST>, do_parse!(i: map_res!(symbol, str::parse::<i32>) >> (AST::I32(i))));
named!(s_nil<AST>, do_parse!(tag!("nil") >> (AST::Nil)));
named!(s_true<AST>, do_parse!(tag!("#t") >> (AST::True)));

named!(unknown_atom<AST>, do_parse!(s : symbol >> (AST::UnknownAtom(s))));

named!(comma_sep_symbols< Vec<&str> >, 
       ws!(
           delimited!(
               tag!("("),
               separated_list!(tag!(","), symbol),
               tag!(")")
               )
           ));

named!(comma_sep< Vec<AST> >, 
       ws!(
           delimited!(
               tag!("("),
               separated_list!(tag!(","), expr),
               tag!(")")
               )
           ));

named!(prim_call<AST>, 
       do_parse!(s: symbol >> args: comma_sep >>
                                (AST::PrimCall(s, args))
                                ));


named!(s_let<AST>, 
       ws!(
       do_parse!(
        tag!("let") >>
        vars: s_let_vars >>
        tag!("in") >>
        body: boxed >>
        (AST::Let(vars, body)))
       )
       );

named!(s_let_vars< Vec<(&str, Box<AST>)> >,
    ws!(separated_nonempty_list!(tag!(","), s_let_var))
);

named!(s_let_var< (&str, Box<AST>) >, ws!(do_parse!(
        name: symbol >>
        tag!("=") >>
        value: boxed >>
        (name, value)
        )));

named!(s_cond<AST>,
       ws!(do_parse!(
        tag!("cond") >>
        tag!("{") >>
        branches: s_cond_branches >>
        tag!("}") >>
        (AST::Cond(branches))
       )));

named!(s_cond_branches< Vec<(Box<AST>, Box<AST>)> >, ws!(separated_nonempty_list!(tag!(","), s_cond_branch)));

named!(s_cond_branch<(Box<AST>, Box<AST>)>, ws!(do_parse!(
        condition: boxed >>
        tag!("=>") >>
        value: boxed >>
        (condition, value)
)));

named!(s_lambda<AST>, ws!(do_parse!(
            vars: comma_sep_symbols >>
            tag!("=>") >>
            body: boxed >>
            (AST::Lambda(vars, body))
        )));

named!(func_call<AST>, 
       do_parse!(
                 tag!("(") >> 
                 s: boxed >>
                 tag!(")") >>
                 tag!("(") >> 
                 args: separated_list!(tag!(","), expr) >>
                 tag!(")") >>
                                (AST::Call(s, args))
                                ));



#[cfg(test)]
mod tests {
    use nom::IResult;
    use super::*;

    #[test]
    fn symbol_test() {
        assert_eq!(symbol(&b"+"[..]), IResult::Done(&b""[..], "+"));
    }

    #[test]
    fn expr_symbol_text() {
        assert_eq!(expr(&b"+;"[..]), IResult::Done(&b";"[..], AST::UnknownAtom("+")));
    }

    #[test]
    fn comma_sep_test() {
        assert_eq!(comma_sep(&b"(2,3)"[..]), IResult::Done(&b""[..], vec![
                                                            AST::I32(2),
                                                            AST::I32(3)

        ]));
    }

    #[test]
    fn prim_call_test() {
        assert_eq!(prim_call(&b"+(2, 3)"[..]), IResult::Done(&b""[..], 
                                                    AST::PrimCall("+", vec![
                                                                 AST::I32(2),
                                                                 AST::I32(3),
                                                    ])
                                                    ));
    }

    #[test]
    fn s_let_test() {
        use nom::Err::Position;

        let result = s_let(&b"let x = 10 in +(x, 5)"[..]);
        {
            match result {
                IResult::Error(Position(ref a, ref b)) => {
                    println!("a: {:?} ; b: {}", a, str::from_utf8(b).unwrap());
                }
                _ => (),
            };
        }
        assert_eq!(result, IResult::Done(&b""[..], 
                                                    AST::Let(vec![
                                                        ("x", Box::new(AST::I32(10)))
                                                    ], Box::new(AST::PrimCall("+", vec![
                                                                    AST::UnknownAtom("x"),
                                                                    AST::I32(5),
                                                    ])))));
    }

    #[test]
    fn s_let_test2() {
        let result = s_let(&b"let x = 10 in x;"[..]);

        assert_eq!(result, IResult::Done(&b";"[..],
                                         AST::Let(vec![("x", Box::new(AST::I32(10)))],
                                            Box::new(AST::UnknownAtom("x")))));
    }

    #[test]
    fn s_cond_test() {
        let result = s_cond(&b"cond{ #t => 1, nil => 2}"[..]);

        {
            use nom::Err::Position;
            match result {
                IResult::Error(Position(ref a, ref b)) => {
                    println!("a: {:?} ; b: {}", a, str::from_utf8(b).unwrap());
                }
                _ => (),
            };
        }
        assert_eq!(result, IResult::Done(&b""[..],
                                         AST::Cond(vec![
                                            (Box::new(AST::True), Box::new(AST::I32(1))),
                                            (Box::new(AST::Nil), Box::new(AST::I32(2))),
                                         ])));
    }

    #[test]
    #[ignore]
    fn s_lambda_test() {
        let result = s_lambda(&b"(x, y) => +(x, y)"[..]);

        assert_eq!(result,
                   IResult::Done(&b""[..],
                                 AST::Lambda(vec!["x", "y"],
                                            Box::new(
                                                AST::PrimCall("+", vec![
                                                             AST::UnknownAtom("x"),
                                                             AST::UnknownAtom("y"),
                                                ])
                                                ))));
    }
}

/*
named!(labels<AST>, do_parens!(tag!("labels") >> l: labels_inner >> body: boxed >>
                            (AST::Labels(l, body))));

named!(labels_inner<Vec<(&str, ASTLabel)> >, parens!(many0!(label_inner)));
named!(label_inner<(&str, ASTLabel)>, parens!(tuple!(symbol, label_code)));
named!(label_code <ASTLabel>, do_parens!(tag!("code") >>
    args: parens!(many0!(symbol)) >>
    free: parens!(many0!(symbol)) >> 
    body: boxed >> 
    (ASTLabel{
        args: args,
        free: free,
        body: body
    })));

named!(closure<AST>, do_parens!(tag!("closure") >> 
    label: symbol >>
    vars: ws!(many0!(sexp)) >>
    (AST::Closure(label, vars))));
*/
