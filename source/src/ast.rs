#[derive(PartialEq)]
pub enum AST<'a> {
    I32(i32), //1
    Nil, //2
    True,

    Let(Vec<(&'a str, Box<AST<'a>>)>, Box<AST<'a>>), //5

    Cond(Vec<(Box<AST<'a>>, Box<AST<'a>>)>), //6

    Lambda(Vec<&'a str>, Box<AST<'a>>), // 10

    UnknownAtom(&'a str),
    PrimCall(&'a str, Vec<AST<'a>>),
    Call(Box<AST<'a>>, Vec<AST<'a>>),
}


use std::fmt;

impl<'a> fmt::Debug for AST<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        //write!(f, "Point {{ x: {}, y: {} }}", self.x, self.y)
        match *self {
            AST::I32(i) => write!(f, "{}", i),
            AST::Nil => write!(f, "nil"),
            AST::True => write!(f, "#t"),
            AST::Let(ref bs, ref body) => {
                write!(f, "let ")?;
                for &(name, ref val) in bs {
                    write!(f, "{} = {:?},", name, val)?;
                }
                write!(f, "\n in \n {:?}", body)
            }
            AST::Cond(ref br) => {
                write!(f, "cond {{\n")?;
                for &(ref cond, ref exp) in br {
                    write!(f, "{:?} => {:?}\n", cond, exp)?;
                }
                write!(f, "}}")
            }
            AST::Lambda(ref args, ref body) => {
                write!(f, "(")?;
                for arg in args {
                    write!(f, "{:?}, ", arg)?;
                }
                write!(f, ") => {:?}", body)
            }
            AST::UnknownAtom(a) => write!(f, "{}", a),
            AST::PrimCall(name, ref args) => {
                write!(f, "primcall[{}](", name)?;
                for arg in args {
                    write!(f, "{:?}, ", arg)?;
                }
                write!(f, ")")
            }
            AST::Call(ref name, ref args) => {
                write!(f, "call[{:?}]", name)?;
                for arg in args {
                    write!(f, "{:?}, ", arg)?;
                }
                write!(f, ")")
            }
        }
    }
}
