use runtime::bytecode::Bytecode;

#[derive(Debug)]
pub enum Lang {
    Byte(Bytecode),
    Closure(usize, usize),
    Goto(usize),
}

use std::collections::HashMap;
#[derive(Debug)]
pub struct Output {
    pub code: Vec<Lang>,
    pub labels: HashMap<usize, usize>,
    pub entry_point: usize,
}

impl Output {
    pub fn new() -> Output {
        Output {
            code: vec![],
            labels: HashMap::new(),
            entry_point: 0,
        }
    }
}

pub struct Pass<'a> {
    __fix: &'a str,
    output: Output,
    emit_count: usize,
}

use passes::environment::Lang as ILang;
use passes::environment::Block as IBlock;
use passes::environment::Output as Input;

impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass {
            __fix: "",
            output: Output::new(),
            emit_count: 0,
        }
    }

    pub fn process(&mut self, input: &'a Input) -> &Output {
        for b in &input.blocks {
            self.process_block(b);
        }
        &self.output
    }

    fn process_block(&mut self, block: &'a IBlock) {
        if let Some(label) = block.label {
            let loc = self.emit_count;
            self.output.labels.insert(label, loc);
        }

        for i in &block.code {
            self.process_input(i);
        }

        if block.procedure.is_some() {
            let loc = self.emit_count;
            self.output.entry_point = loc;
        }
    }

    fn process_input(&mut self, input: &'a ILang) {
        match *input {
            ILang::I32(ref i) => self.emit_bt(Bytecode::PushI32(*i)),
            ILang::Nil => self.emit_bt(Bytecode::PushNil),
            ILang::True => self.emit_bt(Bytecode::PushTrue),
            ILang::IsAtom => self.emit_bt(Bytecode::IsAtom),
            ILang::Add1 => self.emit_bt(Bytecode::Add1),
            ILang::Sub1 => self.emit_bt(Bytecode::Sub1),
            ILang::Not => self.emit_bt(Bytecode::Not),
            ILang::And => self.emit_bt(Bytecode::And),
            ILang::Or => self.emit_bt(Bytecode::Or),
            ILang::Add => self.emit_bt(Bytecode::Add),
            ILang::Sub => self.emit_bt(Bytecode::Sub),
            ILang::Mul => self.emit_bt(Bytecode::Mul),
            ILang::Div => self.emit_bt(Bytecode::Div),
            ILang::Equals => self.emit_bt(Bytecode::IsEqual),
            ILang::Less => self.emit_bt(Bytecode::IsLess),
            ILang::Greater => self.emit_bt(Bytecode::IsGreater),
            ILang::LessOrEq => self.emit_bt(Bytecode::IsLessOrEq),
            ILang::GreaterOrEq => self.emit_bt(Bytecode::IsGreaterOrEq),
            ILang::Pop => self.emit_bt(Bytecode::Pop),
            ILang::LocalVar(offset) => self.emit_bt(Bytecode::Load(offset)),
            ILang::ArgVar(loc) => self.emit_bt(Bytecode::LoadArg(loc)),
            ILang::FreeVar(offset) => {
                self.emit_bt(Bytecode::LoadClosure);
                self.emit_bt(Bytecode::LoadMemOffset(offset));
            }
            ILang::SaveLocalVar(offset) => self.emit_bt(Bytecode::Save(offset)),
            ILang::SkipCond => self.emit_bt(Bytecode::SkipCond),
            ILang::Goto(label) => {
                self.emit(Lang::Goto(label));
                self.emit_count += 1;
            }
            ILang::ConsEl => {
                self.emit_bt(Bytecode::SaveMem);
                self.emit_bt(Bytecode::GetAddr);
            }
            ILang::Cons => {
                self.emit_bt(Bytecode::SaveMem);
                self.emit_bt(Bytecode::GetAddr);
                self.emit_bt(Bytecode::Cons);
            }
            ILang::Car => {
                self.emit_bt(Bytecode::Car);
                self.emit_bt(Bytecode::LoadMem);
            }
            ILang::Cdr => {
                self.emit_bt(Bytecode::Cdr);
                self.emit_bt(Bytecode::LoadMem);
            }
            ILang::Return => self.emit_bt(Bytecode::Return),
            ILang::Closure(label, argc) => {
                self.emit(Lang::Closure(label, argc));
                self.emit_count += 1;
                self.emit_bt(Bytecode::SaveMem);
                self.emit_bt(Bytecode::GetAddr);
            }
            ILang::ClosureArg => self.emit_bt(Bytecode::SaveMem),
            ILang::Call(argc) => self.emit_bt(Bytecode::Call(argc)),
        }
    }

    fn emit(&mut self, code: Lang) {
        self.output.code.push(code);
    }

    fn emit_bt(&mut self, byte: Bytecode) {
        self.emit(Lang::Byte(byte));
        self.emit_count += 1;
    }
}
