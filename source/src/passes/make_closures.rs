#[derive(Clone, Debug)]
pub enum Lang<'a> {
    I32(i32),
    Nil,
    True,

    IsAtom,
    Add1,
    Sub1,
    Not,

    And,
    Or,
    Add,
    Sub,
    Mul,
    Div,
    Equals,
    Less,
    LessOrEq,
    Greater,
    GreaterOrEq,

    PushEnv,
    LetVar(&'a str),
    PopEnv,

    Var(&'a str),

    SaveVar(&'a str),

    SkipCond,
    Goto(usize),

    ConsEl,
    Cons,
    Car,
    Cdr,

    LetProc(&'a str, usize),
    LetArg(&'a str),
    LetFree(&'a str),
    Return,

    Closure(&'a str, usize, usize), //free_argc, argc
    ClosureArg,
    Call(usize),
}

#[derive(Clone)]
pub struct Block<'a> {
    pub label: Option<usize>,
    pub code: Vec<Lang<'a>>,
    pub procedure: Option<usize>,
    pub order: usize,
}

impl<'a> Block<'a> {
    pub fn new() -> Block<'a> {
        Block {
            code: vec![],
            label: None,
            procedure: None,
            order: 0,
        }
    }
}

pub struct Output<'a> {
    pub blocks: Vec<Block<'a>>,
}

impl<'a> Output<'a> {
    pub fn new() -> Output<'a> {
        Output { blocks: vec![] }
    }
}

pub struct Pass<'a> {
    output: Output<'a>,
    block: Block<'a>,
}

use passes::named_lambdas::Output as Input;
use passes::free_variables::Lang as ILang;

impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass {
            output: Output::new(),
            block: Block::new(),
        }
    }

    pub fn process(&mut self, input: &'a Input<'a>) -> &Output<'a> {
        for b in input.blocks.unwrap() {
            self.new_block();
            self.block.label = b.label;
            self.block.procedure = b.procedure;
            self.block.order = b.order;

            for i in &b.code {
                self.process_input(i, input);
            }

            self.push_block();
        }

        &self.output
    }

    fn process_input(&mut self, input: &'a ILang, input_meta: &'a Input<'a>) {
        enum_mapper!(self, input, ILang, Lang;
            Nil, True, IsAtom, Add1, Sub1, Add, Sub, Equals,
            Mul, Div, Not, And, Or, Less, Greater, LessOrEq, GreaterOrEq,
            PushEnv, PopEnv, SkipCond, ConsEl, Cons, Car, Cdr, Return
                     ;
            ILang::I32(ref i) => self.emit(Lang::I32(*i)),
            ILang::LetVar(name) => self.emit( Lang::LetVar(name)),
            ILang::Var(name) => self.emit( Lang::Var(name)),
            ILang::Goto(label) =>  self.emit(Lang::Goto(label)),

            ILang::LetArg(name) => self.emit( Lang::LetArg(name)),

            ILang::BeginProc => {
                self.emit(Lang::PushEnv);

                let label = self.block.label.unwrap();
                let prcs = &input_meta.prcs.unwrap();

                let free_vars = &prcs[&label].free;

                for var in  free_vars {
                    self.emit(Lang::LetFree(var));
                }

            }
            ILang::Lambda(name, label, argc) => {
                self.emit(Lang::LetProc(name, label));

                let prcs = &input_meta.prcs.unwrap();
                let free_vars = &prcs[&label].free;

                let free_argc = free_vars.len();
                
                self.emit(Lang::Closure(name, free_argc, argc));

                let lambda_bindings = &input_meta.lambdas;
                if let Some(lambda_binding) = lambda_bindings[&label] {
                    self.emit(Lang::SaveVar(lambda_binding));
                }

                for var in free_vars {
                    self.emit(Lang::Var(var));
                    self.emit(Lang::ClosureArg);
                }
            },
            ILang::SaveVar(name) => {
                let names = &input_meta.names;
                if names[name].is_none() {
                    self.emit( Lang::SaveVar(name));
                }
                
            }
            ILang::Call(argc) => self.emit( Lang::Call(argc))
        )
    }

    fn emit(&mut self, output: Lang<'a>) {
        self.block.code.push(output);
    }

    fn new_block(&mut self) {
        self.block = Block::new();
    }

    fn push_block(&mut self) {
        let block = self.block.clone();
        self.output.blocks.push(block);
    }
}

use std::fmt;

impl<'a> fmt::Debug for Output<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{\n")?;
        write!(f, "{:#?}", self.blocks)?;
        write!(f, "}}")
    }
}

impl<'a> fmt::Debug for Block<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "\n({}, L: {:?}, P: {:?})\n",
            self.order,
            self.label,
            self.procedure
        )?;
        write!(f, "{:?}", self.code)
    }
}
