pub use passes::free_variables::Output;
pub use passes::free_variables::{Lang, Block, Procedure};

//use std::collections::HashMap;


pub struct Pass<'a> {
    output: Output<'a>,
}

impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass { output: Output::new() }
    }

    pub fn process(&mut self, input: &'a Output<'a>) -> &Output<'a> {

        self.output.prcs = input.prcs.clone();

        let stack = input.prcs.iter().map(|p| *p.0).collect::<Vec<usize>>();

        for top in stack {
            self.process_procedure(top);
        }

        let blocks = &input.blocks.unwrap();
        self.output.blocks = Some(blocks);
        &self.output
    }

    pub fn process_procedure(&mut self, label: usize) {
        let mut prc = {
            self.output.prcs[&label].clone()
        };


        if prc.done {
            return;
        }

        let mut free = prc.free.clone();

        for lambda in &prc.lambdas {
            let label = lambda.1;

            let lambda_prc_done = {
                self.output.prcs[&label].done
            };


            if !lambda_prc_done {
                self.process_procedure(label);
            }

            let lambda_prc = {
                &self.output.prcs[&label].clone()
            }.clone();

            for f in lambda_prc.free {
                if free.contains(&f) {
                    continue;
                }
                if prc.args.contains(&f) {
                    continue;
                }
                free.push(f);
            }
        }

        prc.free = free;
        prc.done = true;
        self.output.prcs.insert(label, prc);
    }
}
