#[derive(Debug, Clone)]
pub enum Lang {
    I32(i32),
    Nil,
    True,

    IsAtom,
    Add1,
    Sub1,
    Not,

    And,
    Or,
    Add,
    Sub,
    Mul,
    Div,
    Equals,
    Less,
    LessOrEq,
    Greater,
    GreaterOrEq,

    Pop,

    LocalVar(i32),
    ArgVar(usize),
    FreeVar(i32),

    SaveLocalVar(i32),

    SkipCond,
    Goto(usize),

    Cons,
    ConsEl,
    Car,
    Cdr,

    Return,

    Closure(usize, usize), //label, free_argc
    ClosureArg,

    Call(usize),
}

#[derive(Debug, Clone)]
pub struct Block {
    pub label: Option<usize>,
    pub code: Vec<Lang>,
    pub procedure: Option<usize>,
}

impl Block {
    pub fn new() -> Block {
        Block {
            code: vec![],
            label: None,
            procedure: None,
        }
    }
}

#[derive(Debug)]
pub struct Output {
    pub blocks: Vec<Block>,
}

impl Output {
    pub fn new() -> Output {
        Output { blocks: vec![] }
    }
}

use std::collections::HashMap;

#[derive(Debug)]
pub struct Env {
    labels: HashMap<String, usize>,
    local_vars: HashMap<String, usize>,
    args: Vec<String>,
    free_vars: Vec<String>,
    stack_pointer: usize,
}

impl Env {
    pub fn new(stack_pointer: usize) -> Env {
        Env {
            labels: HashMap::new(),
            local_vars: HashMap::new(),
            args: vec![],
            free_vars: vec![],
            stack_pointer: stack_pointer,
        }
    }
}

use passes::make_closures::Lang as ILang;
use passes::make_closures::Output as Input;

pub struct Pass<'a> {
    __fix: &'a str,
    output: Output,
    env: Vec<Env>,
    stack_pointer: usize,
    block: Block,
}



enum Var {
    None,
    Local(i32),
    Argument(usize),
    Free(i32),
}

use error::CompilerError;

use std::str::FromStr;
impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass {
            __fix: "",
            output: Output::new(),
            stack_pointer: 0,
            env: vec![],
            block: Block::new(),
        }
    }

    pub fn process(&mut self, input: &'a Input<'a>) -> Result<&Output, CompilerError> {
        for b in &input.blocks {
            self.new_block();
            self.block.label = b.label;
            self.block.procedure = b.procedure;

            for i in &b.code {
                self.process_input(i)?;
            }

            self.push_block();
        }

        Ok(&self.output)
    }

    fn process_input(&mut self, input: &ILang<'a>) -> Result<(), CompilerError> {
        enum_mapper!(self, input, ILang, Lang;
            Nil, True, IsAtom, Add1, Sub1, Add, Sub, Equals,
            Not, And, Or, Mul, Div, Less, LessOrEq, Greater, GreaterOrEq,
            SkipCond, ConsEl, Cons, Car, Cdr, Return, ClosureArg ;

            ILang::I32(ref i) => self.emit(Lang::I32(*i)),

            ILang::Goto(label) => self.emit(Lang::Goto(label)),
            ILang::Call(argc) => {
                self.stack_pointer -= argc;

                self.emit(Lang::Call(argc));
            }

            ILang::PushEnv => {
                let pointer = self.stack_pointer;
                self.env.push(Env::new(pointer));
            }
            ILang::LetVar(name) => {
                let location = self.stack_pointer;
                self.stack_pointer += 1;
                let mut env = self.env.last_mut()
                    .ok_or_else(|| CompilerError::new_str("LETVAR: Couldn't find any environment"))?;
                
                let name = String::from_str(name).map_err(|_| CompilerError::new_str("LETVAR: Couldn't process name."))?;
                env.local_vars.insert(name, location);
            }
            ILang::Var(name) => {
                match self.get_var(name) {
                    Var::Local(loc) => self.emit(Lang::LocalVar(loc)),
                    Var::Argument(loc) => self.emit(Lang::ArgVar(loc)),
                    Var::Free(loc) => self.emit(Lang::FreeVar(loc)),
                    Var::None => {
                        return Err(CompilerError::new(format!("variable `{}` not found", name)));
                    }
                }
            }
            ILang::SaveVar(name) => {
                match self.get_var(name) {
                    Var::Local(loc) => self.emit(Lang::SaveLocalVar(loc)),
                    _ => {
                        return Err(CompilerError::new(format!("variable to save `{}` not found", name)));
                    }
                }
            }
            ILang::PopEnv => {
                let stack_pointer = {
                    let env = &self.env.last().ok_or_else(|| CompilerError::new_str("POPENV: Couldn't find any environment."))?;

                    env.stack_pointer
                };

                let local_vars = self.stack_pointer - stack_pointer;

                self.stack_pointer = stack_pointer;


                if local_vars > 0 {
                    for _ in 0..local_vars - 1 {
                        self.emit(Lang::Pop);
                    }
                }

                self.env.pop();
            }

            ILang::LetProc(name, label) => {
                let mut env = self.env.last_mut().ok_or_else(|| CompilerError::new_str("LETPROC: Couldn't find any environment."))?;
                let name = String::from_str(name).map_err(|_| CompilerError::new_str("LETPROC: Couldn't process name."))?;
                env.labels.insert(name, label);
            }
            ILang::LetArg(name) => {
                let mut env = self.env.last_mut().ok_or_else(|| CompilerError::new_str("LETARG: Couldn't find any environment."))?;
                let name = String::from_str(name).map_err(|_| CompilerError::new_str("LETARG: Couldn't process name."))?;
                env.args.push(name);
            }
            ILang::LetFree(name) => {
                let mut env = self.env.last_mut().ok_or_else(|| CompilerError::new_str("LETFREE: Couldn't find any environment."))?;
                let name = String::from_str(name).map_err(|_| CompilerError::new_str("LETFREE: Couldn't process name."))?;
                env.free_vars.push(name);
            }
            ILang::Closure(name, free_argc, _) => {
                let label = match self.get_label(name) {
                    Some(l) => l,
                    None => return Err(CompilerError::new(format!("function `{}` not found", name)))
                };

                self.emit(Lang::Closure(label, free_argc));
            }
        );
        Ok(())
    }

    fn get_label(&self, s: &str) -> Option<usize> {
        for env in self.env.iter().rev() {
            if let Some(location) = env.labels.get(s) {
                return Some(*location);
            }
        }
        None
    }

    fn get_var(&self, s: &str) -> Var {
        for env in self.env.iter().rev() {
            if let Some(location) = env.local_vars.get(s) {
                return Var::Local(*location as i32);
            }

            if let Some(location) = env.args.iter().enumerate().find(|r| r.1 == s) {
                return Var::Argument(location.0);
            }

            if let Some(location) = env.free_vars.iter().enumerate().find(|r| r.1 == s) {
                return Var::Free((location.0 + 1) as i32);
            }
        }
        Var::None
    }

    fn emit(&mut self, code: Lang) {
        self.block.code.push(code);
    }

    fn new_block(&mut self) {
        self.block = Block::new();
    }

    fn push_block(&mut self) {
        let block = self.block.clone();
        self.output.blocks.push(block);
    }
}
