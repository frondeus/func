pub use passes::block::{Lang, Block, Output};

pub struct Pass<'a> {
    output: Output<'a>,
}

type Input<'a> = &'a Output<'a>;

impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass { output: Output::new() }
    }

    pub fn process(&mut self, input: Input<'a>) -> &Output<'a> {
        for b in &input.blocks {
            self.output.blocks.push(b.clone());
        }

        self.output.blocks.sort_by(|b1, b2| {
            chain_ordering(
                b1.procedure.cmp(&b2.procedure).reverse(),
                b1.order.cmp(&b2.order),
            )
        });

        for (i, block) in self.output.blocks.iter_mut().enumerate() {
            block.order = i;
        }

        &self.output
    }
}

use std::cmp::Ordering;

fn chain_ordering(o1: Ordering, o2: Ordering) -> Ordering {
    match o1 {
        Ordering::Equal => o2,
        _ => o1,
    }
}
