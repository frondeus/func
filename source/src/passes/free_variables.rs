use passes::reorder::Output as Input;
pub use passes::reorder::{Lang, Block};

use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct Procedure<'a> {
    pub done: bool,
    pub label: usize,
    pub args: Vec<&'a str>,
    pub free: Vec<&'a str>,
    pub lambdas: Vec<(&'a str, usize)>,
}

pub struct Output<'a> {
    pub blocks: Option<&'a Vec<Block<'a>>>,
    pub prcs: HashMap<usize, Procedure<'a>>,
}

impl<'a> Output<'a> {
    pub fn new() -> Output<'a> {
        Output {
            blocks: None,
            prcs: HashMap::new(),
        }
    }
}


pub struct Pass<'a> {
    output: Output<'a>,
    prc: Option<Procedure<'a>>,
}

impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass {
            output: Output::new(),
            prc: None,
        }
    }

    pub fn process(&mut self, input: &'a Input<'a>) -> &Output<'a> {
        let blocks = &input.blocks;

        for b in blocks {
            for i in &b.code {
                self.process_input(i, b);
            }
        }

        self.output.blocks = Some(blocks);
        &self.output
    }

    fn process_input(&mut self, input: &'a Lang, block: &'a Block<'a>) {
        match *input {
            Lang::BeginProc => {
                if let Some(label) = block.label {
                    self.prc = Some(Procedure {
                        done: true,
                        label: label,
                        args: vec![],
                        free: vec![],
                        lambdas: vec![],
                    });
                }
            }

            Lang::LetVar(name) |
            Lang::LetArg(name) => {
                if let Some(ref mut prc) = self.prc {
                    prc.args.push(name);
                }
            }

            Lang::Var(name) => {
                if let Some(ref mut prc) = self.prc {
                    let free = !prc.args.contains(&name) && !prc.free.contains(&name);

                    if free {
                        prc.free.push(name);
                    }
                }
            }

            Lang::Lambda(name, label, _) => {
                if let Some(ref mut prc) = self.prc {
                    prc.lambdas.push((name, label));
                    prc.done = false;
                }
            }

            Lang::Return => {
                let prc = self.prc.clone();

                if let Some(ref prc) = prc {
                    let label = prc.label;
                    self.output.prcs.insert(label, prc.clone());
                }

                self.prc = None;
            }
            _ => (),
        }
    }
}

use std::fmt;

impl<'a> fmt::Debug for Output<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{\n")?;
        if let Some(bl) = self.blocks {
            write!(f, "{:#?}", bl)?;
        } else {
            write!(f, "No blocks")?;
        }
        write!(f, "\nprcs: {:#?}", self.prcs)?;
        write!(f, "}}")
    }
}
