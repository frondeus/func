#[derive(Clone, Debug)]
pub enum Lang<'a> {
    I32(i32),
    Nil,
    True,

    IsAtom,
    Add1,
    Sub1,
    Not,

    And,
    Or,
    Add,
    Sub,
    Mul,
    Div,
    Equals,
    Less,
    LessOrEq,
    Greater,
    GreaterOrEq,

    PushEnv,
    LetVar(&'a str),
    SaveVar(&'a str),
    PopEnv,

    Var(&'a str),

    SkipCond,
    Goto(usize),

    Cons,
    ConsEl,
    Car,
    Cdr,

    LetArg(&'a str),

    BeginProc,
    Return,

    Lambda(&'a str, usize, usize), // label, argc
    Call(usize),
}

#[derive(Clone)]
pub struct Block<'a> {
    pub label: Option<usize>,
    pub code: Vec<Lang<'a>>,
    pub procedure: Option<usize>,
    pub order: usize,
}

impl<'a> Block<'a> {
    pub fn new() -> Block<'a> {
        Block {
            code: vec![],
            label: None,
            procedure: None,
            order: 0,
        }
    }
}

pub struct Output<'a> {
    pub blocks: Vec<Block<'a>>,
}

impl<'a> Output<'a> {
    pub fn new() -> Output<'a> {
        Output { blocks: vec![] }
    }
}

pub struct Pass<'a> {
    output: Output<'a>,
    current_block: Block<'a>,
    prc: Vec<usize>,
}

use passes::flat::Lang as ILang;
use passes::flat::Output as Input;


impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass {
            output: Output::new(),
            current_block: Block::new(),
            prc: vec![],
        }
    }

    pub fn process(&mut self, input: &'a Input<'a>) -> &Output<'a> {
        for i in &input.code {
            self.process_input(i)
        }

        self.new_block();

        for (i, block) in self.output.blocks.iter_mut().enumerate() {
            block.order = i;
        }

        &self.output
    }

    fn process_input(&mut self, input: &'a ILang<'a>) {
        enum_mapper!( self, input, ILang, Lang ;
                Nil, True,
                IsAtom, Add1, Sub1, Not,
                And, Or, Add, Sub, Mul, Div, Equals, Less, LessOrEq, Greater, GreaterOrEq,
                PushEnv, PopEnv,
                ConsEl, Cons, Car, Cdr
                ;

            ILang::LetVar(name) => self.emit(Lang::LetVar(name)),
            ILang::Var(name) => self.emit(Lang::Var(name)),
            ILang::SaveVar(name) => self.emit(Lang::SaveVar(name)),
            ILang::LetArg(name) => self.emit(Lang::LetArg(name)),
            ILang::Call(argc) => self.emit(Lang::Call(argc)),

            ILang::I32(ref i) => self.emit(Lang::I32(*i)),
            ILang::Lambda(name, label, argc) => self.emit(Lang::Lambda(name, label, argc)),

            ILang::Label(label) => {
                let is_empty = self.current_block.code.is_empty();
                if !is_empty || self.current_block.label.is_some() {
                    self.new_block();
                }
                self.current_block.label = Some(label);
            }
            ILang::SkipCond => {
                self.emit(Lang::SkipCond);
                self.new_block();
            }
            ILang::Goto(label) => {
                self.emit(Lang::Goto(label));
                self.new_block();
            }
            ILang::BeginProc(label) => {
                self.prc.push(label);
                self.new_block();
                self.current_block.label = Some(label);
                self.current_block.procedure = Some(label);
                self.emit(Lang::BeginProc);
            }
            ILang::Return => {
                self.emit(Lang::Return);
                self.prc.pop();
                self.new_block();
            }

        )
    }

    fn new_block(&mut self) {
        let block = self.current_block.clone();
        self.output.blocks.push(block);
        self.current_block = Block::new();

        let procedure = self.prc.last().cloned();
        self.current_block.procedure = procedure;
    }

    fn emit(&mut self, code: Lang<'a>) {
        self.current_block.code.push(code);
    }
}


use std::fmt;

impl<'a> fmt::Debug for Output<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:#?}", self.blocks)
    }
}

impl<'a> fmt::Debug for Block<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "\n({}, L: {:?}, P: {:?})\n",
            self.order,
            self.label,
            self.procedure
        )?;
        write!(f, "{:?}", self.code)
    }
}
