use passes::expand_free::Output as Input;
pub use passes::expand_free::{Lang, Block, Procedure};

use std::collections::HashMap;

pub struct Output<'a> {
    pub blocks: Option<&'a Vec<Block<'a>>>,
    pub prcs: Option<&'a HashMap<usize, Procedure<'a>>>,
    pub lambdas: HashMap<usize, Option<&'a str>>,
    pub names: HashMap<&'a str, Option<usize>>,
}

impl<'a> Output<'a> {
    pub fn new() -> Output<'a> {
        Output {
            blocks: None,
            prcs: None,
            lambdas: HashMap::new(),
            names: HashMap::new(),
        }
    }
}

pub struct Pass<'a> {
    output: Output<'a>,
    last_lambda: Option<usize>, //label
}

impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass {
            output: Output::new(),
            last_lambda: None,
        }
    }

    pub fn process(&mut self, input: &'a Input<'a>) -> &Output<'a> {
        let blocks = input.blocks;
        let prcs = &input.prcs;

        for b in blocks.unwrap() {
            for i in &b.code {
                self.process_input(i);
            }
        }

        self.output.blocks = blocks;
        self.output.prcs = Some(prcs);

        &self.output
    }

    pub fn process_input(&mut self, input: &'a Lang) {
        match *input {
            Lang::Lambda(_, label, _) => {
                self.output.lambdas.insert(label, None);
                self.last_lambda = Some(label);
            }
            Lang::SaveVar(name) => {
                self.output.names.insert(name, None);

                if name != "__ret" {
                    if let Some(lambda_label) = self.last_lambda {
                        self.output.lambdas.insert(lambda_label, Some(name));
                        self.output.names.insert(name, Some(lambda_label));
                    }
                }
                self.last_lambda = None;
            }
            _ => {
                self.last_lambda = None;
            }
        }
    }
}

use std::fmt;


impl<'a> fmt::Debug for Output<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{{\n")?;
        if let Some(bl) = self.blocks {
            write!(f, "{:#?}", bl)?;
        } else {
            write!(f, "No blocks")?;
        }
        write!(f, "\nprcs: {:#?}", self.prcs)?;
        write!(f, "\nlambdas: {:#?}", self.lambdas)?;
        write!(f, "}}")
    }
}
