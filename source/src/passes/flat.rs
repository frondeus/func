#[derive(Debug)]
pub enum Lang<'a> {
    I32(i32),
    Nil,
    True,

    IsAtom,
    Add1,
    Sub1,
    Not,

    And,
    Or,
    Add,
    Sub,
    Mul,
    Div,
    Equals,
    Less,
    LessOrEq,
    Greater,
    GreaterOrEq,

    PushEnv,
    LetVar(&'a str),
    PopEnv,

    Var(&'a str),

    SaveVar(&'a str),

    Label(usize),
    SkipCond,
    Goto(usize),

    ConsEl,
    Cons,
    Car,
    Cdr,

    BeginProc(usize),
    LetArg(&'a str),
    Return,

    Lambda(&'a str, usize, usize), //label, argc
    Call(usize),
}

pub struct Output<'a> {
    pub code: Vec<Lang<'a>>,
}

use ast;
type ILang<'a> = ast::AST<'a>;
type Input<'a> = &'a [ILang<'a>];


pub struct Pass<'a> {
    output: Output<'a>,
    label_count: usize,
}

impl<'a> Output<'a> {
    pub fn new() -> Output<'a> {
        Output { code: vec![] }
    }
}

use error::CompilerError;

impl<'a> Pass<'a> {
    pub fn new() -> Pass<'a> {
        Pass {
            output: Output::new(),
            label_count: 0,
        }
    }

    pub fn process(&mut self, input: Input<'a>) -> Result<&Output<'a>, CompilerError> {
        for i in input {
            self.process_input(i)?;
        }

        Ok(&self.output)
    }

    fn assert_args(
        varname: &'a str,
        args: &'a [ILang<'a>],
        number: usize,
    ) -> Result<(), CompilerError> {
        if args.len() == number {
            return Ok(());
        }

        let msg = format!(
            "Function `{}` requires {} arguments but {} were provided.",
            varname,
            number,
            args.len()
        );

        Err(CompilerError::new(msg))
    }

    fn process_input(&mut self, input: &'a ILang) -> Result<(), CompilerError> {
        match *input {
            ast::AST::I32(i) => self.emit(Lang::I32(i)),
            ast::AST::Nil => self.emit(Lang::Nil),
            ast::AST::True => self.emit(Lang::True),
            ast::AST::UnknownAtom(a) => self.emit(Lang::Var(a)),
            ast::AST::PrimCall("atom?", ref args) => {
                Self::assert_args("atom?", args, 1)?;
                self.process_input(&args[0])?;
                self.emit(Lang::IsAtom);
            }
            ast::AST::PrimCall("inc", ref args) => {
                Self::assert_args("inc", args, 1)?;
                self.process_input(&args[0])?;
                self.emit(Lang::Add1);
            }
            ast::AST::PrimCall("dec", ref args) => {
                Self::assert_args("dec", args, 1)?;
                self.process_input(&args[0])?;
                self.emit(Lang::Sub1);
            }
            ast::AST::PrimCall("not", ref args) => {
                Self::assert_args("not", args, 1)?;
                self.process_input(&args[0])?;
                self.emit(Lang::Not);
            }
            ast::AST::PrimCall("and", ref args) => {
                Self::assert_args("and", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::And);
            }
            ast::AST::PrimCall("or", ref args) => {
                Self::assert_args("or", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Or);
            }
            ast::AST::PrimCall("+", ref args) => {
                Self::assert_args("+", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Add);
            }
            ast::AST::PrimCall("-", ref args) => {
                Self::assert_args("-", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Sub);
            }
            ast::AST::PrimCall("*", ref args) => {
                Self::assert_args("*", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Mul);
            }
            ast::AST::PrimCall("/", ref args) => {
                Self::assert_args("/", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Div);
            }
            ast::AST::PrimCall("==", ref args) => {
                Self::assert_args("==", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Equals);
            }
            ast::AST::PrimCall("<", ref args) => {
                Self::assert_args("<", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Less);
            }
            ast::AST::PrimCall(">", ref args) => {
                Self::assert_args(">", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::Greater);
            }
            ast::AST::PrimCall("<=", ref args) => {
                Self::assert_args("<=", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::LessOrEq);
            }
            ast::AST::PrimCall(">=", ref args) => {
                Self::assert_args(">=", args, 2)?;
                self.process_input(&args[0])?;
                self.process_input(&args[1])?;
                self.emit(Lang::GreaterOrEq);
            }
            ast::AST::Let(ref bindings, box ref body) => {
                self.emit(Lang::PushEnv);
                self.emit(Lang::Nil);
                self.emit(Lang::LetVar("__ret"));

                for &(name, ref value) in bindings {
                    self.emit(Lang::Nil);
                    self.emit(Lang::LetVar(name));

                    self.process_input(value)?;
                    self.emit(Lang::SaveVar(name));
                }

                self.process_input(body)?;
                self.emit(Lang::SaveVar("__ret"));
                self.emit(Lang::PopEnv);
            }
            ast::AST::Cond(ref branches) => {
                let end_label = self.new_label();
                let mut next_label = self.new_label();

                for &(ref condition, ref body) in branches {
                    self.emit(Lang::Label(next_label));
                    next_label = self.new_label();
                    self.process_input(condition)?;
                    self.emit(Lang::SkipCond);
                    self.emit(Lang::Goto(next_label));
                    self.emit(Lang::PushEnv);
                    self.process_input(body)?;
                    self.emit(Lang::PopEnv);
                    self.emit(Lang::Goto(end_label));
                }
                self.emit(Lang::Label(next_label));
                self.emit(Lang::Nil);
                self.emit(Lang::Label(end_label));
            }
            ast::AST::PrimCall("cons", ref args) => {
                assert!(args.len() == 2);
                self.process_input(&args[0])?;
                self.emit(Lang::ConsEl);
                self.process_input(&args[1])?;
                self.emit(Lang::Cons);
            }
            ast::AST::PrimCall("car", ref args) => {
                assert!(args.len() == 1);
                self.process_input(&args[0])?;
                self.emit(Lang::Car);
            }
            ast::AST::PrimCall("cdr", ref args) => {
                assert!(args.len() == 1);
                self.process_input(&args[0])?;
                self.emit(Lang::Cdr);
            }
            ast::AST::Lambda(ref vars, ref body) => {
                self.process_lambda(vars, body)?;
            }
            ast::AST::PrimCall(name, ref args) => {
                self.process_prim_call(name, args)?;
            }
            ast::AST::Call(box ref name, ref args) => {
                self.process_call(name, args)?;
            } 
        };

        Ok(())
    }

    fn process_prim_call(
        &mut self,
        varname: &'a str,
        args: &'a [ILang<'a>],
    ) -> Result<(), CompilerError> {
        self.emit(Lang::PushEnv);
        for arg in args {
            self.process_input(arg)?;
            self.emit(Lang::LetVar("__call_arg"));
        }

        let argc = args.len();

        self.emit(Lang::Var(varname));
        self.emit(Lang::Call(argc));
        self.emit(Lang::PopEnv);
        Ok(())
    }

    fn process_call(
        &mut self,
        closure: &'a ILang<'a>,
        args: &'a [ILang<'a>],
    ) -> Result<(), CompilerError> {
        self.emit(Lang::PushEnv);

        self.emit(Lang::Nil);
        self.emit(Lang::LetVar("__ret"));
        for arg in args {
            self.process_input(arg)?;
            self.emit(Lang::LetVar("__call_arg"));
        }


        let argc = args.len();

        self.process_input(closure)?;

        self.emit(Lang::Call(argc));
        self.emit(Lang::SaveVar("__ret"));
        self.emit(Lang::PopEnv);
        Ok(())
    }

    fn process_lambda(
        &mut self,
        args: &'a [&'a str],
        body: &'a ILang<'a>,
    ) -> Result<(), CompilerError> {
        let name = "tmp_lambda_todo";

        let next_label = self.new_label();
        self.emit(Lang::Lambda(name, next_label, args.len()));
        self.emit(Lang::BeginProc(next_label));

        for arg in args {
            self.emit(Lang::LetArg(arg));
        }

        self.process_input(body)?;

        self.emit(Lang::PopEnv);
        self.emit(Lang::Return);
        Ok(())
    }

    fn emit(&mut self, output: Lang<'a>) {
        self.output.code.push(output);
    }

    fn new_label(&mut self) -> usize {
        self.label_count += 1;
        self.label_count
    }
}





use std::fmt;

impl<'a> fmt::Debug for Output<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self.code)
    }
}
