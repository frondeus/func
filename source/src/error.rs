
pub struct CompilerError {
    text: String,
}

impl CompilerError {
    pub fn new_str(text: &str) -> CompilerError {
        CompilerError { text: String::from(text) }
    }
    pub fn new(text: String) -> CompilerError {
        CompilerError { text: text }
    }
}

use std::fmt;

impl fmt::Debug for CompilerError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.text)
    }
}
