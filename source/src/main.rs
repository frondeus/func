extern crate funclib;
extern crate env_logger;

use funclib::repl;

fn main() {
    env_logger::init().unwrap();

    repl();
}
