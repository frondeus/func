macro_rules! mkvariant {
    ($l: ident, $pat: ident) =>
        ($l::$pat)
}

#[macro_export]
macro_rules! enum_mapper {
    ($sel: ident,
     $input: expr, 
     $l1: ident,
     $l2: ident
     ; $( $enum_variant: ident ),* ;
     $($rest: tt)*) => {
        match *$input {
            $(
                mkvariant!($l1, $enum_variant) => $sel.emit(mkvariant!($l2, $enum_variant))
            ),*
            ,$($rest)*
        }
    }
}
