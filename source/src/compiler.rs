pub trait Pass {
    type Input;
    type Output;
    fn process(&mut self, input: Self::Input) -> &Self::Output;
}

use passes::{flat, block, reorder, free_variables, expand_free, named_lambdas, make_closures,
             environment, codegen};
use passes::codegen::Output as CodegenPassOutput;
use passes::codegen::Lang as LabelsBytecode;


use runtime::bytecode::Bytecode;
use ast::AST;
use error::CompilerError;

pub struct Compiler {
    pub output: Vec<Bytecode>,
    pub entry_point: usize,
}


impl Compiler {
    pub fn new() -> Compiler {
        Compiler {
            output: vec![],
            entry_point: 0,
        }
    }

    pub fn compile(&mut self, input: &[AST]) -> Result<(), CompilerError> {
        let mut flat = flat::Pass::new();
        let mut block = block::Pass::new();
        let mut reorder = reorder::Pass::new();
        let mut free = free_variables::Pass::new();
        let mut expand = expand_free::Pass::new();
        let mut named = named_lambdas::Pass::new();
        let mut closures = make_closures::Pass::new();
        let mut env = environment::Pass::new();
        let mut code = codegen::Pass::new();

        debug!("L1: {:?} \n", input);

        let f = flat.process(input)?;
        debug!("Flat {:?}\n", f);

        let b = block.process(f);
        debug!("Block {:?}\n", b);

        let r = reorder.process(b);
        debug!("Reorder {:#?}\n", r);

        let fr = free.process(r);
        debug!("Free {:#?}\n", fr);

        let e = expand.process(fr);
        debug!("Expand {:#?}\n", e);

        let n = named.process(e);
        debug!("Named {:#?}\n", n);

        let c = closures.process(n);
        debug!("Closures {:?}\n", c);

        let en = env.process(c)?;
        debug!("Env {:?}\n", en);

        let cg = code.process(en);
        debug!("Codegen {:?}\n", cg);

        for c in &cg.code {
            self.compile_cg(c, cg);
        }

        self.entry_point = cg.entry_point;
        debug!(
            "Result {{ entry_point: {:?}, code: {:?} }}\n",
            self.entry_point,
            self.output
        );

        Ok(())
    }

    fn compile_cg(&mut self, code: &LabelsBytecode, input: &CodegenPassOutput) {
        match *code {
            LabelsBytecode::Byte(ref b) => self.output.push(b.clone()),
            LabelsBytecode::Goto(ref label) => {
                let label_loc = input.labels[label];
                self.emit(Bytecode::Jump(label_loc));
            }
            LabelsBytecode::Closure(ref label, ref argc) => {
                let label_loc = input.labels[label];
                self.emit(Bytecode::PushClosure(label_loc, *argc));
            }
            //ref c => unimplemented!("bytecode: {:?}", c),
        }
    }

    fn emit(&mut self, byte: Bytecode) {
        self.output.push(byte);
    }
}
