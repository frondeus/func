#![feature(slice_patterns)]
#![feature(box_patterns)]
#![feature(test)]


#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]

extern crate test;

#[macro_use]
extern crate nom;
extern crate timebomb;

#[macro_use]
extern crate log;

extern crate runtime;

#[macro_use]
mod macros;

mod error;
mod ast;
mod parser;
mod passes;
mod compiler;

pub mod driver;

use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::str;
use std::string::String;

use nom::IResult;
use nom::Err::Position;

fn eval_input(ast: ast::AST) {
    use runtime::bytecode::Bytecode;
    use runtime::vm::VirtualMachine;

    let mut compiler = compiler::Compiler::new();
    if let Err(err) = compiler.compile(&[ast]) {
        error!("{:?}", err);
        return ();
    }

    let mut bytes = compiler.output.clone();
    let entry_point = compiler.entry_point;
    bytes.push(Bytecode::Print);
    bytes.push(Bytecode::Halt);

    let mut printer = ReplPrinter::new();
    let mut vm = VirtualMachine::new();

    vm.eval(&mut printer, bytes, entry_point);
}

fn read_input(input: &str, stdout: &mut io::Stdout) -> Option<String> {
    match parser::expr(input.as_bytes()) {
        IResult::Done(completed, ast) => {
            eval_input(ast);

            if !completed.is_empty() {
                let s = str::from_utf8(completed).unwrap();
                Some(s.to_owned())
            } else {
                None
            }
        }
        IResult::Incomplete(_) => None,
        IResult::Error(err) => {
            match err {
                Position(ref err_pos, err_input) => {
                    let err_msg = str::from_utf8(err_input).unwrap();
                    error!("Error:");
                    error!("{:?} -- {}", err_pos, err_msg);
                    stdout.flush().unwrap();
                    None
                }
                ref el => {
                    error!("{:?}", el);
                    panic!()
                }
            }
        }
    }
}

pub fn repl() {

    let mut input = String::new();
    let stdin = io::stdin();
    let mut stdout = io::stdout();

    print!("> ");
    stdout.flush().unwrap();

    let reader = BufReader::new(stdin);

    for input_line in reader.lines() {
        input += &input_line.unwrap();

        loop {
            if let Some(new_input) = read_input(&input, &mut stdout) {
                input = new_input.clone();
            } else {
                print!("> ");
                stdout.flush().unwrap();
                break;
            }

            print!("> ");
            stdout.flush().unwrap();
        }

    }
}

use runtime::vm::Printer;
use runtime::value::Value;

struct ReplPrinter;

impl Printer for ReplPrinter {
    fn print(&mut self, value: Value) {
        println!("{:?}", value);
    }
}

impl ReplPrinter {
    fn new() -> ReplPrinter {
        ReplPrinter {}
    }
}

#[cfg(test)]
mod tests {
    use runtime::value::Value;
    use driver::assert_driver;

    use test::Bencher;
    use test::black_box;

    fn fib_native(x: usize) -> usize {
        match x {
            0 => 0,
            1 => 1,
            x => fib_native(x - 1) + fib_native(x - 2),
        }
    }

    fn fib_func(x: i32, y: i32) {
        assert_driver(
            &format!(
                "let fib = (x) =>
                    cond {{
                        ==(x, 0) => 0,
                        ==(x, 1) => 1,
                        #t => +(fib(dec(x)), fib(-(x, 2)))
                    }}
                in fib({})",
                x
            ),
            &Value::I32(y),
        );
    }

    #[test]
    fn test_fib_native10() {
        assert_eq!(fib_native(10), 55);
    }

    #[bench]
    fn bench_fib_func5(b: &mut Bencher) {
        b.iter(|| { fib_func(5, 5); });
    }

    #[bench]
    fn bench_fib_native5(b: &mut Bencher) {
        b.iter(|| fib_native(black_box(5)));
    }

    #[bench]
    fn bench_fib_func10(b: &mut Bencher) {
        b.iter(|| { fib_func(10, 55); });
    }

    #[bench]
    fn bench_fib_native10(b: &mut Bencher) {
        b.iter(|| fib_native(black_box(10)));
    }

    #[bench]
    fn bench_fib_func15(b: &mut Bencher) {
        b.iter(|| { fib_func(15, 610); });
    }

    #[bench]
    fn bench_fib_native15(b: &mut Bencher) {
        b.iter(|| fib_native(black_box(15)));
    }

    #[bench]
    fn bench_fib_func20(b: &mut Bencher) {
        b.iter(|| { fib_func(20, 6765); });
    }

    #[bench]
    fn bench_fib_native20(b: &mut Bencher) {
        b.iter(|| fib_native(black_box(20)));
    }

    #[bench]
    fn bench_fib_func25(b: &mut Bencher) {
        b.iter(|| { fib_func(25, 75025); });
    }

    #[bench]
    fn bench_fib_native25(b: &mut Bencher) {
        b.iter(|| fib_native(black_box(25)));
    }
}
